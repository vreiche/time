﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour
{

    //fill out all public variables in editor before testing
    //enemy needs sight start and sight end child game objects to be able to see

    //movement variables
    public float movementspeed;
    public float playerDistance;
    public Transform player;
    Renderer enemyRenderer;
    bool engage = false;
    public GameObject enemySprite;
    public bool spriteFacingRight = true;
    Rigidbody2D rigid;
    
    //line of sight variables
    public Transform sightStart, sightEnd;
    public bool spotted;
    public float escapeDistance;

    

    //ground check variables
    public LayerMask enemyMask;
    public float patrolspeed;
    public bool grounded;
    public float groundCheckDistance;

    //wall check variables
    public bool wallhit;
    public bool wallSpotted;

    //animation
    //public Animator enemyAnimator;


    void Start()
    {

        enemyRenderer = GetComponent<Renderer>();
        rigid = GetComponent<Rigidbody2D>();
        //InvokeRepeating("lookAround", 0f, Random.Range(1, 10));
        //enemyAnimator = GetComponent<Animator>();

    }

    void FixedUpdate()
    {
        //for testing purposes
        playerDistance = Vector3.Distance(player.position, transform.position);

        //movement calls
        Raycasting();
        engagePlayer();
        chasePlayer();

        //test zone
        patrol();
        wallCheck();
    }
    
    void Raycasting()
    {
        //spotted and check for wall in spotted range raycast, start and end position are determined by childed game objects in enemy's game object
        sightStart.transform.position = new Vector3(sightStart.transform.position.x, sightEnd.transform.position.y, sightEnd.transform.position.z);
        sightEnd.transform.position = new Vector3(sightEnd.transform.position.x, sightEnd.transform.position.y, sightEnd.transform.position.z);
        Debug.DrawLine(sightStart.transform.position, sightEnd.transform.position, Color.green);

        spotted = Physics2D.Linecast(sightStart.transform.position, sightEnd.transform.position, 1 << LayerMask.NameToLayer("Player"));
        wallSpotted = Physics2D.Linecast(sightStart.transform.position, sightEnd.transform.position, 1 << LayerMask.NameToLayer("Walls"));


        //ground check raycast, starting position is wherever sight start is placed.. looks to collide with a 'Floor' layer
        Vector2 lineCastPosition = sightStart.position;
        Debug.DrawLine(lineCastPosition, lineCastPosition + new Vector2(0, groundCheckDistance));

        grounded = Physics2D.Linecast(lineCastPosition, lineCastPosition + new Vector2(0, groundCheckDistance), 1 << LayerMask.NameToLayer("Floor"));


        //wallcheck raycast, starting position will be is wherever sight start is placed. looks to collide with 'Walls' layer
        float facingXdir = enemySprite.transform.localScale.x;
        facingXdir *= 1f;
        Vector2 lineCastPos = sightStart.transform.position;
        Debug.DrawLine(lineCastPos, lineCastPos + new Vector2(facingXdir, 0f), Color.red);

        wallhit = Physics2D.Linecast(lineCastPos, lineCastPos + new Vector2(0, 0), 1 << LayerMask.NameToLayer("Walls"));
        

    }

    void chasePlayer()
    {
        if (engage == true)
        {


            if (spriteFacingRight && rigid.transform.position.x < player.position.x)
            {
                chasingPlayer();
            }

            if (spriteFacingRight && rigid.transform.position.x > player.position.x)
            {
                turnFacing();
            }
            
            if (!spriteFacingRight && rigid.transform.position.x < player.position.x)
            {
                turnFacing();
            }

            if (!spriteFacingRight && rigid.transform.position.x > player.position.x)
            {
                chasingPlayer();
            }

        }

    }

    void turnFacing()
    {
        float facingX = enemySprite.transform.localScale.x;
        facingX *= -1f;
        enemySprite.transform.localScale = new Vector3(facingX, enemySprite.transform.localScale.y, enemySprite.transform.localScale.z);
        spriteFacingRight = !spriteFacingRight;
        rigid.velocity = new Vector2(0f, 0f);
        Debug.Log("facing player");

        //fast turning animation would add polish
        //enemyAnimator.SetBool("isTurning", turning);
    }

    void chasingPlayer()
    {
        if (!spriteFacingRight)
        {
            rigid.AddForce(new Vector2(-1, 0) * movementspeed);
            Debug.Log("chasing left");
        }

        if (spriteFacingRight)
        {
            rigid.AddForce(new Vector2(1, 0) * movementspeed);
            Debug.Log("chasing right");
        }

        //enemyAnimator.SetBool("isCharging", charging);
        Debug.Log("chasing player");
    }

    void lookAround()
    {
        if (!spotted && !engage)
        {
            turnFacing();
        }
        //enemyAnimator.SetBool("isIdle", idle);
    }

    void engagePlayer()
    {
        if (spotted && !wallSpotted)
        {
            engage = true;
            //enemyAnimator.SetBool("isSpotting", spotting);

        }
        else if (playerDistance > escapeDistance)
        {
            engage = false;
        }
    }

    void patrol()
    {
        //could easily add a gap jumping enemy here.. replacing turnfacing with a jump
        if (!grounded && !spotted && !engage)
        {
            turnFacing();
        }

        if (!spriteFacingRight && !spotted && !engage)
        {
            rigid.AddForce(new Vector2(-1, 0) * patrolspeed);
            Debug.Log("chasing left");
        }

        if (spriteFacingRight && !spotted && !engage)
        {
            rigid.AddForce(new Vector2(1, 0) * patrolspeed);
            Debug.Log("chasing right");
        }
        //enemyAnimator.SetBool("isWalking", walking);
    }

    void wallCheck()
    {
        if(wallhit)
        {
            turnFacing();
            Debug.Log("wall detected turning around");
        }
    }
        
}
