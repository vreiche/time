﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (MyController))]
public class Movement : MonoBehaviour {

	private float h;
	public float jumpHeight = 4;
	public float timeToJumpApex = .4f;
	float accelerationTimeAirborne =.2f;
	float accelerationTimeGrounded =.1f;

	float gravity;
	Vector3 velocity;
	public float moveSpeed = 6;
	float jumpVelocity;
	float velocityXsmoothing;
	private Transform mytran;
	bool ledgeleft = false;
	bool ledgeright = false;


	MyController mycontroller;

	// Use this for initialization
	void Start () {
		mycontroller = GetComponent<MyController> ();
		gravity = -(2 * jumpHeight)/Mathf.Pow (timeToJumpApex, 2);
		jumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
		print ("Gravity: " + gravity + "jump velocity: " + jumpVelocity);
		mytran = this.transform;
	}
	
	// Update is called once per frame
	void Update () {

		if(mycontroller.collisions.above || mycontroller.collisions.below) {
			velocity.y = 0;
		}

		h = Input.GetAxisRaw("Horizontal");
		Vector2 input = new Vector2(h, Input.GetAxisRaw("Vertical"));

		if(Input.GetKeyDown(KeyCode.Space) && mycontroller.collisions.below) {
			velocity.y = jumpVelocity;
		}
			
		float targetVelocityX = input.x * moveSpeed;
		velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXsmoothing, (mycontroller.collisions.below)?accelerationTimeGrounded:accelerationTimeAirborne);
		velocity.y += gravity * Time.deltaTime;
		mycontroller.Move (velocity * Time.deltaTime);

		if(Input.GetKey(KeyCode.LeftShift)) {
			moveSpeed = 12;
		}
		else {
			moveSpeed = 6;
		}

		if(ledgeleft == true && Input.GetKey("up")){
			//Climbing animation goes here.
			transform.Translate(-3,3,0);
			ledgeleft = false;
		}
		if(ledgeright == true && Input.GetKey("up")){
			//Climbing animation goes here.
			transform.Translate(3,3,0);
			ledgeright = false;
		}
		if(ledgeright == true || ledgeleft == true){
			if ( Input.GetKey("down")) {
				gravity = -(2 * jumpHeight)/Mathf.Pow (timeToJumpApex, 2);
				ledgeleft = false;
				ledgeright = false;
			}
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.tag == "ledge" && mycontroller.collisions.left) {
			velocity.x = 0;
			velocity.y = 0;
			gravity = 0;
			ledgeleft = true;
		}
		if (other.gameObject.tag == "ledge" && mycontroller.collisions.right) {
			velocity.x = 0;
			velocity.y = 0;
			gravity = 0;
			ledgeright = true;
		}
	}

	void OnTriggerExit2D (Collider2D other) {
		gravity = -(2 * jumpHeight)/Mathf.Pow (timeToJumpApex, 2);
	}

	IEnumerator wait () {
		yield return new WaitForSeconds(1f);
	}
}
