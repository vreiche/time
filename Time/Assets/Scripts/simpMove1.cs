﻿using UnityEngine;
using System.Collections;

public class simpMove1 : MonoBehaviour {

	public float moveSpeed;
	public float jump;
	private Rigidbody2D rb;
	float old_pos;
	bool isIdle;
	bool iswalkr;
	bool iswalkl;
	bool isjogr;
	bool isjogl;
	bool isrunr;
	bool isrunl;

	// Use this for initialization
	void Start () {
		rb = gameObject.GetComponent<Rigidbody2D>();
		isIdle = true;
		iswalkr = false;
		iswalkl =false;
		isjogr = false;
		isjogl = false;
		isrunr = false;
		isrunl = false;
	}
	
	void Update () {
		if (Input.GetKey("j")) {
			Time.timeScale = 1;
		}
		else {
			Time.timeScale = 0.03F;
		}
		checkme();
		moving();
		if (Input.GetKeyDown("space")) {
			rb.AddForce(new Vector2(0,jump), ForceMode2D.Impulse);
		}
	}


	void FixedUpdate () {
//		checkme();
//		moving();
	}
	void checkme() {
		if (isIdle == true && Input.GetKeyDown("d")) {
			isIdle = false;
			iswalkr = true;
		}
		else if (iswalkr == true && Input.GetKeyDown("d")) {
			iswalkr = false;
			isjogr = true;
		}
		else if (isjogr == true && Input.GetKeyDown("d")) {
			isjogr = false;
			isrunr = true;
		}
		else if (iswalkr == true && Input.GetKeyDown("a")) {
			iswalkr = false;
			isIdle = true;
		}
		else if (isjogr == true && Input.GetKeyDown("a")) {
			isjogr = false;
			isIdle = true;
		}
		else if (isrunr == true && Input.GetKeyDown("a")) {
			isrunr = false;
			isIdle = true;
			numba();
		}
		else if (isIdle == true && Input.GetKeyDown("a")) {
			isIdle = false;
			iswalkl = true;
		}
		else if (iswalkl == true && Input.GetKeyDown("a")) {
			iswalkl = false;
			isjogl = true;
		}
		else if (isjogl == true && Input.GetKeyDown("a")) {
			isjogl = false;
			isrunl = true;
		}
		else if (iswalkl == true && Input.GetKeyDown("d")) {
			iswalkl = false;
			isIdle = true;
		}
		else if (isjogl == true && Input.GetKeyDown("d")) {
			isjogl = false;
			isIdle = true;
		}
		else if (isrunl == true && Input.GetKeyDown("d")) {
			isrunl = false;
			isIdle = true;
		}
	}
	void moving() {
		if (isIdle == true){
			Vector3 movement = new Vector3(0,0,0);
		}
		else if (isIdle == false && iswalkr == true) { 
			transform.position += new Vector3(moveSpeed * Time.deltaTime,0,0);
		}
		else if (isIdle == false && isjogr == true) { 
			transform.position += new Vector3(2*moveSpeed * Time.deltaTime,0,0);
		}
		else if (isIdle == false && isrunr == true) { 
			transform.position += new Vector3(3*moveSpeed * Time.deltaTime,0,0);
		}
		else if (isIdle == false && iswalkl == true) { 
			transform.position += new Vector3(-moveSpeed * Time.deltaTime,0,0);
		}
		else if (isIdle == false && isjogl == true) { 
			transform.position += new Vector3(-2*moveSpeed * Time.deltaTime,0,0);
		}
		else if (isIdle == false && isrunl == true) { 
			transform.position += new Vector3(-3*moveSpeed * Time.deltaTime,0,0);
		}

	}

	IEnumerator numba () {
		yield return new WaitForSeconds(.5F);
	}

}
