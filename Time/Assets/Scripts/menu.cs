﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class menu : MonoBehaviour {

	public string scene1;
	public string scene2;
	public string currentscene;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Restart () {
		SceneManager.LoadScene (currentscene);
	}

	public void Scene1 () {
		SceneManager.LoadScene (scene1);
	}

	public void Scene2 () {
		SceneManager.LoadScene (scene2);
	}
}
