﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InGameMenuManager : MonoBehaviour {

    public GameObject menuCanvas;
    public GameObject mainmenuPanel;
    public GameObject audiomenuPanel;
    public GameObject helpPanel;

    Transform mainmenuPaneltransform;
    Transform menuCanvastransform;

    public bool menuOpen;


    void Awake()
    {
        //FindPanels();
        //DontDestroyOnLoad(gameObject);
        menuOpen = false;
    }
	// Use this for initialization
	void Start ()
    {
        Time.timeScale = 1;
	   menuCanvas.SetActive(false);
	}
	
	// Update is called once per frame
	void Update ()
    {
        OpenMenu();
	}

    public void Resume()
    {
        menuOpen = false;
        Time.timeScale = 1;
        menuCanvas.SetActive(false);
    }

    /*void FindPanels()
    {
        mainmenuPaneltransform = transform.Find("MainMenuPanel");
        mainmenuPanel = mainmenuPaneltransform.GetComponentInParent<GameObject>();
        menuCanvastransform = transform.Find("MenuCanvas");
        menuCanvas = menuCanvastransform.GetComponentInParent<GameObject>();
    }*/

    void OpenMenu()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && menuOpen==false)//Time.timeScale == 1)
        {
            menuCanvas.SetActive(true);
            mainmenuPanel.SetActive(true);
            Time.timeScale = 0;
            menuOpen = true;
            //Debug.Log("open menu");
        }
        else if (Input.GetKeyDown(KeyCode.Escape) && menuOpen==true)//Time.timeScale == 0)
        {
            Time.timeScale = 1;
            menuCanvas.SetActive(false);
            audiomenuPanel.SetActive(false);
            helpPanel.SetActive(false);
            menuOpen = false;
            //Debug.Log("close menu");
        }
    }
}
