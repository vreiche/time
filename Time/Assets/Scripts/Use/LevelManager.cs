﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour {


    //This script is to make it so that the player cannot access levels that are not available to them ad so that they may access levels that should be available to them
    /*Could do where getting to end of level or somewhere unlocks level,so effectively;    */

    //Using Buttons for loading levels in this model.  Can Change later
    /*public Button levelButton01;
    public Button levelButton02;
    public Button levelButton03;

   
    //Trying a list, something not done before by me
    int numberoflevels;
    
    //void Awake()
    //{
       //numberoflevels = SceneManager.sceneCountInBuildSettings; //This includes all scenes, it will be easier to just include all scenes and personally add auto-unlokced levels
    //}
    
    public bool levelUnlocked;

    public List<int> Unlockedlevels = new List<int>();

    void Start()
    {
        CreatePlayerPrefsForLevels();  //have to call before initialunlockedlevellist or list won't compile correctly.
        InitialUnlockedLevelList();
    }

    void Update()
    {
        CheckUnlocked();
    }

    public void CreatePlayerPrefsForLevels()
    {
        for (int i = numberoflevels; i == numberoflevels; i++)
        {
            PlayerPrefs.SetInt("Level_" + i.ToString(), 0);
        }
    }

    public void InitialUnlockedLevelList()
    {
        PlayerPrefs.SetInt("Level_0", 1);
        PlayerPrefs.SetInt("Level_1", 1);
        PlayerPrefs.SetInt("Level_2", 1);


        Unlockedlevels.Add(0);
        Unlockedlevels.Add(1);
        Unlockedlevels.Add(2);
    }
    

    public void CheckUnlocked()
    {
        if (PlayerPrefs.GetInt("Level_2") == 1)
        {
            levelButton01.interactable = true;
            //Level is unlocked and able to play
        }
        else
        {
            levelButton01.interactable = false;
        }
        if (PlayerPrefs.GetInt("Level_3") == 1)
        {
            levelButton02.interactable = true;
            Debug.Log("You can play level 2");
        }
        else
        {
            levelButton02.interactable = false;
            Debug.Log("You cannot play level 2");
        }
    }

    public void UnlockLevel(int level)
    {
        PlayerPrefs.SetInt("Level_" + level.ToString(), 1);
        Unlockedlevels.Add(SceneManager.GetActiveScene().buildIndex);
    }

    void Awake()
    {
        
    }
    */
    public void SetLevel(int level) //Called when a level is completed/a new scene is entered (associate a level number with each level)
    {
        PlayerPrefs.SetInt("CurrentLevel", level);
        PlayerPrefs.Save();
        Debug.Log(level);
    }

    public void LoadLevel()  //Called when play button is pressed
    {
        SceneManager.LoadScene(PlayerPrefs.GetInt("CurrentLevel") + 2);   //Level one is scene two
       
    }

    /*const string LEVEL_KEY = "level_unlocked_";

    

    string currentlevel;

    void SetLevel()
    {
        currentlevel = SceneManager.GetActiveScene().ToString();
        SceneManager.GetSceneAt(1);
    }

    void UnlockLevel()
    {

    }

    void LevelUnlocked(int leveltoUnlock)
    {
        //if the level is unlocked then allow player to access
    }
    void test()
    {
        for (int i = numberoflevels; i <= numberoflevels; i++ )
            {
            if (PlayerPrefs.GetInt(LEVEL_KEY + level.ToString()))
            }

    }

    public static bool IsLevelUnlocked(int level)
    {
        int levelValue = PlayerPrefs.GetInt(LEVEL_KEY + level.ToString());
        bool isLevelUnlocked = (levelValue == 1);

        if (level <= SceneManager.sceneCountInBuildSettings - 1)
        {
            return isLevelUnlocked;
        }
        else
        {
            Debug.LogError("trying to query level not in build order");
            return false;
        }
    }

    public static void UnlockLevel(int level)
    {
        if (level <= SceneManager.sceneCountInBuildSettings - 1)
        {
            PlayerPrefs.SetInt(LEVEL_KEY + level.ToString(), 1); //Use 1 for true
        }
        else
        {
            Debug.LogError("trying to unlock level not in build order");
        }

    }
        */
    }





