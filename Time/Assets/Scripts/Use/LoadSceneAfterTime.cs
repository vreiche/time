﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class LoadSceneAfterTime : MonoBehaviour {

    float timer;
    float timeToSwitch;

	// Use this for initialization
	void Start ()
    {
        timer = 0;
        timeToSwitch = 5;
	}
	
	// Update is called once per frame
	void Update ()
    {
        timer += Time.deltaTime;
        LoadLevelTime();
        LoadLevelClick();
	}

    void LoadLevelClick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            SceneManager.LoadScene(1);
        }
    }

    void LoadLevelTime()
    {
        if (timer >= timeToSwitch)
        {
            SceneManager.LoadScene(1);
        }
        
    }

}
