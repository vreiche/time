﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour {
    GameObject character;
    Rigidbody rb;
    CharacterController charcontoller;

	// Use this for initialization
	void Start () {
        character = GetComponent<GameObject>();
        rb = GetComponent<Rigidbody>();
        charcontoller = GetComponent<CharacterController>();
	}
	
    /*public void MoveChar ()
    {
        if (Input.GetKey("A"))
        {
            charcontoller.Move()
        }
    }*/
    public float speed = 5f;
    void Update()
    {
        var move = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"),0);
        transform.position += move * speed * Time.deltaTime;
    }
}
