﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TurnPanelOff : MonoBehaviour {

    public GameObject panel;

	// Use this for initialization
	void Start ()
    {
        panel.SetActive(false);
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}
}
