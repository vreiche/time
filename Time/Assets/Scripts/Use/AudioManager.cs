﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AudioManager : MonoBehaviour {
    //public AudioSource bgmusicSource;
    //public AudioSource SFXSource;
    //can do another audio manager for the background music also, will just have to change the code some
    GameObject audiomanager;


    public Slider musicSlider;
    public Slider sfxSlider;
    public Slider masterSlider;
    static bool slidersSet;
    
    /*public void OnValueChange ()
    {
        bgmusicSource.volume = musicSlider.value;
    }
    public void TestFunction ()
    {

    }*/

    public AudioClip[] levelMusicChangeArray;

    private AudioSource audioSource;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        Debug.Log("Dont Destroy on load: " + name);
        slidersSet = false;
    }

    // Use this for initialization
    void Start()
    {
        audioSource = GetComponent<AudioSource>();

        //audiomanager = this.gameObject;
    }

    //When a level is loaded calls setvolumes, sets the audioclip for the level and plays that audioclip on a loop
    void OnLevelWasLoaded(int level)
    {
        SetSliders();
        SetVolumes();
        AudioClip thisLevelMusic = levelMusicChangeArray[level];
        Debug.Log("Playing clip: " + thisLevelMusic);
        Debug.Log("Level was loaded");

        //if theres some music attached to persistent music manager
        if (thisLevelMusic)
        {
            Debug.Log("audiosourcechosen");
            audioSource.clip = thisLevelMusic;
            audioSource.loop = true;
            audioSource.Play();
        }
    }

    //Sets the sliders each level so that assuming using the same menu system the sliders will always work, also adds listeners to the sliders
    public void SetSliders()
    {
        musicSlider = GameObject.FindGameObjectWithTag("MusicSlider").GetComponent<Slider>();
        sfxSlider = GameObject.FindGameObjectWithTag("SFXSlider").GetComponent<Slider>();
        masterSlider = GameObject.FindGameObjectWithTag("MasterSlider").GetComponent<Slider>();
        musicSlider.onValueChanged.AddListener(delegate { ChangeMusicValue(); });
        sfxSlider.onValueChanged.AddListener(delegate { ChangeSFXValue(); });
        masterSlider.onValueChanged.AddListener(delegate { ChangeMasterVolume(); });
    }

    //Sets the volumes of the audiosource and the levels of the sliders on load so that the music level decisions are always persistent
    public void SetVolumes()
    {
        if (slidersSet == true)
        {
            sfxSlider.value = PlayerPrefs.GetFloat("sfxVolume");

            musicSlider.value = PlayerPrefs.GetFloat("bgmusicVolume");

            audioSource.volume = PlayerPrefs.GetFloat("bgmusicVolume");

            masterSlider.value = PlayerPrefs.GetFloat("masterVolume");

            sfxSlider.maxValue = PlayerPrefs.GetFloat("masterVolume");

            musicSlider.maxValue = PlayerPrefs.GetFloat("masterVolume");
            Debug.Log("Volumes for sliders are set");
        }
    }

    //Changing of master volume (changes maximum values of music and sfx sliders)
    public void ChangeMasterVolume()
    {

        PlayerPrefs.SetFloat("masterVolume", masterSlider.value);
        AudioListener.volume = masterSlider.value;

        /*
        //SFXSource.volume = masterSlider.value;
        sfxSlider.maxValue = masterSlider.value;
        PlayerPrefs.SetFloat("sfxVolume", sfxSlider.value);

        //bgmusicSource.volume = masterSlider.value;
        musicSlider.maxValue = masterSlider.value;
        PlayerPrefs.SetFloat("bgmusicVolume", musicSlider.value);
        */


        slidersSet = true;
    }

    //Changes value of SFXaudiosource if there's an sfx audiosource, this could be put in another instance of this script that is modified
    public void ChangeSFXValue()
    {
        //SFXSource.volume = sfxSlider.value;
        PlayerPrefs.SetFloat("sfxVolume", sfxSlider.value);
        slidersSet = true;
    }

    //changes music audiosource volume value dependent on level of a slider
    public void ChangeMusicValue()
    {
        audioSource.volume = musicSlider.value;
        PlayerPrefs.SetFloat("bgmusicVolume", musicSlider.value);

        slidersSet = true;
    }



    // Update is called once per frame
    void Update ()
    {

	}
}
