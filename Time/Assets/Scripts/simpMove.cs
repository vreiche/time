﻿using UnityEngine;
using System.Collections;

public class simpMove : MonoBehaviour {

	public float moveSpeed;
	public float jump;
	private Rigidbody2D rb;
	float old_pos;

	// Use this for initialization
	void Start () {
		rb = gameObject.GetComponent<Rigidbody2D>();
		old_pos = transform.position.x;
		Time.timeScale = 0.03F;
	}
	
	void Update () {
		if (Input.anyKey) {
			Time.timeScale = 1;
		}
		else {
			Time.timeScale = 0.03F;
		}
		if (Input.GetKeyDown("space")) {
			rb.AddForce(new Vector2(0,jump), ForceMode2D.Impulse);
		}
	}


	void FixedUpdate () {
		transform.position += new Vector3(Input.GetAxisRaw("Horizontal") * moveSpeed * Time.deltaTime, 0, 0);
	}
}
